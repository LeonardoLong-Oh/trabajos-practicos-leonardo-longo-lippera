package pantallas;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Pantalla5 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblCategoria;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla5 window = new Pantalla5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Pantalla5() {
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(216, 191, 216));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese la categoria");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel.setBounds(35, 67, 169, 20);
		frame.getContentPane().add(lblNewLabel);
		
		txt = new JTextField();
		txt.setBounds(231, 67, 130, 26);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setBackground(new Color(240, 255, 240));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				char hijo = 'a';
				char padres = 'b';
				char abuelos = 'c';
				
				String h = Character.toString(hijo);
				String p = Character.toString(padres);
				String c = Character.toString(abuelos);
				
				String categoria = txt.getText();
				
				if (categoria.equals(h)) {
					lblCategoria.setText("Hijo");}
				 
				else if (categoria.equals(p))
					lblCategoria.setText("Padres");
				
				else 
					lblCategoria.setText("Abuelos");
				
				
				
			}
				
			});
	
		
		btnNewButton.setFont(new Font("Arial Black", Font.PLAIN, 15));
		btnNewButton.setBounds(38, 131, 119, 38);
		frame.getContentPane().add(btnNewButton);
		
		lblCategoria = new JLabel("Categoria");
		lblCategoria.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategoria.setFont(new Font("Arial Black", Font.PLAIN, 17));
		lblCategoria.setBackground(new Color(240, 255, 240));
		lblCategoria.setOpaque(true);
		lblCategoria.setBounds(189, 136, 205, 27);
		frame.getContentPane().add(lblCategoria);
	}
}
