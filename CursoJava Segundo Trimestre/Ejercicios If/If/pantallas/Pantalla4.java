package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla4 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla4 window = new Pantalla4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Pantalla4() {
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(47, 79, 79));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngresarNumeroDel = new JLabel("Ingrese el mes (numero)");
		lblIngresarNumeroDel.setForeground(new Color(255, 255, 255));
		lblIngresarNumeroDel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblIngresarNumeroDel.setBounds(31, 37, 198, 22);
		frame.getContentPane().add(lblIngresarNumeroDel);
		
		txt = new JTextField();
		txt.setBounds(254, 39, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBackground(new Color(250, 250, 210));
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero==1)
					lblResultado.setText("Enero tiene 31 dias");
				else if (numero ==2)
					lblResultado.setText("Febrero tiene 28 dias");
				else if (numero == 3)
					lblResultado.setText("Marzo tiene 31 dias");
				else if (numero == 4)
					lblResultado.setText("Abril tiene 30 dias");
				else if (numero ==5)
					lblResultado.setText("Mayo tiene 31 dias");
				else if (numero == 6)
					lblResultado.setText("Junio tiene 30 dias");
				else if (numero == 7)
					lblResultado.setText("Julio tiene 31 dias");
				else if (numero ==8)
					lblResultado.setText("Agosto tiene 31 dias");
				else if (numero == 9)
					lblResultado.setText("Septiembre tiene 30 dias");
				else if (numero == 10)
					lblResultado.setText("Octubre tiene 31 dias");
				else if (numero ==11)
					lblResultado.setText("Noviembre tiene 30 dias");
				else if (numero == 12)
					lblResultado.setText("Diciembre tiene 31 dias");
				
			}
		});
		btnCalcular.setForeground(new Color(0, 0, 0));
		btnCalcular.setFont(new Font("Arial Black", Font.BOLD, 13));
		btnCalcular.setBounds(31, 115, 97, 25);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(new Color(250, 235, 215));
		lblResultado.setBounds(161, 115, 224, 30);
		frame.getContentPane().add(lblResultado);
	}
}
