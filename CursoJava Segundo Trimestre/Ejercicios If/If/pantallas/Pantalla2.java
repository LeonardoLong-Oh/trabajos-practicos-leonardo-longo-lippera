package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla2 {

	private JFrame frame;
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txt3;
	private JLabel lblPromedio;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Pantalla2() {
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(new Color(245, 245, 220));
		frame.getContentPane().setBackground(new Color(240, 255, 240));
		frame.setBounds(100, 100, 685, 391);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNota = new JLabel("Nota 1 ");
		lblNota.setBackground(new Color(128, 128, 128));
		lblNota.setForeground(new Color(128, 128, 128));
		lblNota.setFont(new Font("Arial Black", Font.PLAIN, 20));
		lblNota.setBounds(30, 63, 79, 25);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_1 = new JLabel("Nota 2");
		lblNota_1.setForeground(new Color(128, 128, 128));
		lblNota_1.setFont(new Font("Arial Black", Font.PLAIN, 20));
		lblNota_1.setBounds(30, 133, 79, 25);
		frame.getContentPane().add(lblNota_1);
		
		JLabel lblNota_2 = new JLabel("Nota 3");
		lblNota_2.setForeground(new Color(128, 128, 128));
		lblNota_2.setFont(new Font("Arial Black", Font.PLAIN, 20));
		lblNota_2.setBounds(30, 198, 79, 33);
		frame.getContentPane().add(lblNota_2);
		
		txt1 = new JTextField();
		txt1.setBounds(119, 68, 116, 22);
		frame.getContentPane().add(txt1);
		txt1.setColumns(10);
		
		txt2 = new JTextField();
		txt2.setText("");
		txt2.setBounds(117, 137, 116, 22);
		frame.getContentPane().add(txt2);
		txt2.setColumns(10);
		
		txt3 = new JTextField();
		txt3.setBounds(117, 206, 116, 22);
		frame.getContentPane().add(txt3);
		txt3.setColumns(10);
		
		JButton btnCalcularPromedio = new JButton("Calcular Promedio");
		btnCalcularPromedio.setBackground(Color.MAGENTA);
		btnCalcularPromedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				float fnota1 = Float.parseFloat(txt1.getText());
				float fnota2 = Float.parseFloat(txt2.getText());
				float fnota3 = Float.parseFloat(txt3.getText());
				
			float fpromedio = (fnota1 + fnota2 + fnota3)/3;
			
			if (fpromedio>=7) {
				lblPromedio.setText("Aprobado") ;
			}
				else lblPromedio.setText("Desaprobado");{
			}
				
				
				
			}
		});
		btnCalcularPromedio.setFont(new Font("Arial Black", Font.PLAIN, 15));
		btnCalcularPromedio.setForeground(new Color(0, 0, 0));
		btnCalcularPromedio.setBounds(314, 63, 205, 42);
		frame.getContentPane().add(btnCalcularPromedio);
		
		lblPromedio = new JLabel("    Promedio");
		lblPromedio.setFont(new Font("Arial Black", Font.PLAIN, 17));
		lblPromedio.setOpaque(true);
		lblPromedio.setBackground(new Color(221, 160, 221));
		lblPromedio.setForeground(new Color(240, 255, 240));
		lblPromedio.setBounds(360, 130, 131, 33);
		frame.getContentPane().add(lblPromedio);
	}
}
