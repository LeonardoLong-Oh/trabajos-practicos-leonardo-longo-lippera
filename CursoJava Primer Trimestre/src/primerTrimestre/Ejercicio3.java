package primerTrimestre;

public class Ejercicio3 {

	public static void main(String[] args) {

		System.out.println("Tecla de Escape \t\t\t Significado");
		System.out.println("\n");
		System.out.println("\\n \t\t\t\t\t Significa nueva lnea");
		System.out.println("\\t \t\t\t\t\t Significa un tab de espacio");
		System.out.println("\\\" \t\t\t\t\t Es para poner \"(comillas donles) dentro del texto por ejemplo");
		System.out.println("\t\t\t\t\t \"Bebecita\"");
		System.out.println("\\\\ \t\t\t\t\t Se utiliza para escribir la \\ dentro del texto, por ejemplo \\esto\\ ");
		System.out.println("\\\' \t\t\t\t\t Se utiliza para la \'(comilla simple) para escribir por ejemplo \'Cactus\'");


	}

}
